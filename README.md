# New_blog

This project is intended to be an evolution of the Blog project carried out during my initial training as a full-stack web developer.
The technologies used for this realization are:
* Php 7.4 (API REST crud)
* Symfony 4.4
* Twig
* MariaDB
